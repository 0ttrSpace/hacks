# Upload Vulnerabilities
## Tutorial room exploring some basic file-upload vulnerabilities in websites
#### - Ryan Kohalmy (aka 0ttrSpace)

Enter this code to set hosts file for this room
```bash
echo "10.10.251.121    overwrite.uploadvulns.thm shell.uploadvulns.thm java.uploadvulns.thm annex.uploadvulns.thm magic.uploadvulns.thm jewel.uploadvulns.thm" | sudo tee -a /etc/hosts
```

To remove the added code to hosts use:
```bash
sudo sed -i '$d' /etc/hosts
```

The purpose of this room is to explore some of the vulnerabilities resulting from improper (or inadequate) handling of file uploads. Specifically, we will be looking at:

  - Overwriting existing files on a server
  - Uploading and Executing Shells on a server
  - Bypassing Client-Side filtering
  - Bypassing various kinds of Server-Side filtering
  - Fooling content type validation checks

Like with any hack we should first focus on enumeration. For file upload knowing where the files are going and what we can upload. For this you can use dir brutefocing with Gobuster, capture requests with Burpsuit, and Wapalyzer (browser extension) can provide useful info. Once we have some info, like if the app uses client-side or server-side uploading, and we can poke at that to learn where the holes are.

In this 'super secure' example you can see that an image on the site is stroed in a specific location and it's name. In this instance we can upload a file with the same name and change the image on the site. It will not be this easy when preforming a real life situation (ie during a pentest). 

![example page source](https://i.imgur.com/BeqAZ3s.png)

For our practicle we will be visitng the site `overwrite.uploadvulns.thm.`

We need to change the site's image to get the flag. If we look at the site's source code we see that the images for this site are stored in the images directory. 

![practicle site code](https://i.ibb.co/R9d1pNT/Screen-Shot-2021-10-07-at-3-58-30-PM.png)

So, let's grab an image to replace it with. Rename your file and upload it to get the flag for this example. 

![flag for room](https://i.ibb.co/rbN67Xx/Screen-Shot-2021-10-07-at-4-02-22-PM.png)


